package controllers;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Vector;
import static play.data.Form.*;

import models.Contestent;
import models.GenerateContest;
import models.Identification;
import models.Identification.Votant;
import play.*;
import play.api.mvc.Session;
import play.data.Form;
import play.data.validation.ValidationError;
import play.mvc.*;
import play.mvc.Http.Cookie;

public class Application extends Controller {

	public static final int year = 2013;
	public static final File folder = new File("public/images/imgINSA_"
			+ Application.year);

	static int nbMess = 0;
	static int id = 0;
	static HashMap<Integer, String> mdps = new HashMap<Integer, String>();
	static int numImgCurr = 0;
	static int nbLineVote = 0;
	static int nbCellVote = 1;

	public static Result getYearSpe(String year_spe) {
		return ok(views.html.template.year_constest_template.render(Contestent
				.copyIterator(Contestent.findBySpe(0, year_spe).iterator()),
				year_spe));
	}

	public static Result index() {
		return ok(views.html.index.render("Salut", form(Identification.class)));

	}

	public static Result generateContest(Integer tour) {
		new GenerateContest(tour);
		return ok("Contest generate for tour #" + tour);

	}

	public static Result auth() {
		Form<Identification> filledForm = form(Identification.class)
				.bindFromRequest();

		for (Entry<String, List<ValidationError>> entry : filledForm.errors()
				.entrySet()) {
			System.out.println("-- " + entry.getKey() + "/" + entry.getValue());
		}

		for (Entry<String, String> entry : filledForm.data().entrySet()) {
			System.out
					.println("--> " + entry.getKey() + "/" + entry.getValue());
		}
		System.out.println();

		if (filledForm.hasErrors()) {
			System.out.println("Errrrrrrrrrrrrrrrrrrr");
			session("err", "y");
			return redirect(routes.Application.index());
		} else {
			int id_u = id++;
			session("id", "" + id_u);
			Identification ident = filledForm.get();
			ident.name = filledForm.data().get("name");
			String mdp = Identification.auth(ident);
			if (mdp != null) {
				mdps.put(id_u, mdp);
				System.out.println("id : " + id_u + " mdp : " + mdp + " name: "
						+ filledForm.data().get("name"));
				session("err", null);

				play.mvc.Http.Session session = Http.Context.current()
						.session();
				session.put("name", ident.name);

				return ok(views.html.auth.render(form(Identification.class)));
			} else {
				session("err", "y");
				return redirect(routes.Application.index());
			}
		}
	}

	public static Result welcome() {
		Form<Identification> filledForm = form(Identification.class)
				.bindFromRequest();
		if (filledForm.hasErrors()) {
			System.out.println("Errrrrrrrrrrrrrrrrrrr22222");
			session("err", "y");
			return redirect(routes.Application.index());
		} else {
			play.mvc.Http.Session session = Http.Context.current().session();

			return ok(views.html.welcome.render(session.get("name")));
		}

	}

	public static Result handleResult(int idContest1, int idContest2, int idVote) {
		return ok();
	}

	public static Result nbMess(Integer id) {
		Application.nbMess++;
		response().setCookie("nbMess", "" + id);
		return ok();
	}

	public static Result updateScores(Boolean incrScore1, String c1, String c2) {
		System.out.println("----------------->  Here " + c1);
		Contestent c = Contestent.findByName(c1, c2, 0);
		play.mvc.Http.Session session = Http.Context.current().session();

		float pourcentage;
		String strResult;
		String name = session.get("name");

		if (!Identification.hasAlreadyVoted(name, c.id)) {
			System.out.println("-----------------> " + c);
			if (incrScore1) {
				c.incrementScoreContestent1();
			} else {
				c.incrementScoreContestent2();
			}
			c = Contestent.findByName(c1, c2, 0);
			Identification.insertVotant(new Identification.Votant(name, c.id));
		}
		pourcentage = (float) ((float) c.scoreC1 / (float) (c.scoreC1 + c.scoreC2)) * 100;
		strResult = (int) pourcentage + "::" + (100 - (int) pourcentage) + "::"
				+ c.scoreC1 + "::" + c.scoreC2;

		return ok(strResult);
	}

	private static String createLines(int tour, String spe) {

		String str = "<tr>";
		int numTmp = Application.numImgCurr;
		Iterable<Contestent> constestents = Contestent.findBySpe(tour, spe);
		Iterator<Contestent> cts = constestents.iterator();
		Contestent c;
		int i = 0;
		int j, k;
		int indiceC2Null;

		while (cts.hasNext()) {
			indiceC2Null = -1;
			Application.nbCellVote = 1;
			for (j = 0; j < 3 && cts.hasNext(); j++) {
				c = cts.next();
				str += "<td class=\"span1\" align=\"center\">"
						+ "	<img id = \""
						+ spe
						+ "_"
						+ (numTmp++)
						+ "\" alt=\""
						+ c.imgConstes1
						+ "\" title=\""
						+ c.imgConstes1
						+ "\" src=\"/assets/images/imgINSA_"
						+ Application.year
						+ "/"
						+ c.imgConstes1
						+ "\" class=\"img-polaroid\">"
						+ "</td>"
						+ "<td class=\"span1\" align=\"center\">"
						+ "    <img src=\"/assets/images/VS.png\" class=\"img-circle\">"
						+ "</td>";
				if (c.imgConstes2 != null) {
					str += "<td class=\"span1\" align=\"center\">"
							+ "	<img id = \"" + spe + "_" + (numTmp++)
							+ "\" alt=\"" + c.imgConstes2 + "\" title=\""
							+ c.imgConstes2
							+ "\" src=\"/assets/images/imgINSA_"
							+ Application.year + "/" + c.imgConstes2
							+ "\" class=\"img-polaroid\">" + "</td>";
				} else {
					indiceC2Null = j;
				}
				if (j < 2) {
					str += "<td class=\"span1\"  align=\"center\">"
							+ "	<img src=\"/assets/images/vr.png\">" + "</td>";
				}
			}

			str += "</tr><tr id=\"" + Application.nbLineVote + "\">";
			numTmp = Application.numImgCurr;
			for (k = 0; k < 3 && k < j; k++) {
				str += "<td class=\"span1\" align=\"center\">"
						+ "<a id = \"b"
						+ spe
						+ "_"
						+ (numTmp)
						+ "\" class=\"btn btn-inverse\" onclick=\"checkBTN('"
						+ spe
						+ "_"
						+ (numTmp++)
						+ "');\" ><i class=\"icon-thumbs-up icon-white\"></i></a>"
						+ "</td>" + "<td class=\"span1\" align=\"center\">"
						+ "<a class=\"btn btn-danger \" onclick=\"checkVote("
						+ (Application.nbLineVote) + ","
						+ (Application.nbCellVote) + " )\" >Vote</a>" + "</td>";
				Application.nbCellVote += 4;
				// if (indiceC2Null != -1) {
				str += "<td class=\"span1\" align=\"center\">"
						+ "<a id = \"b"
						+ spe
						+ "_"
						+ (numTmp)
						+ "\" class=\"btn btn-inverse\" onclick=\"checkBTN('"
						+ spe
						+ "_"
						+ (numTmp++)
						+ "');\" ><i class=\"icon-thumbs-up icon-white\"></i></a>"
						+ "</td>";
				// }
				if (k < 2) {
					str += "<td class=\"span1\"  align=\"center\">" + "</td>";
				}
			}

			str += "</tr><tr>";
			for (i = 0; i < 11; i++) {
				str += "<td class=\"span1\"><hr></td>";
			}
			i = -1;
			Application.numImgCurr = numTmp;
			str += "</tr><tr>";
			i++;
			Application.nbLineVote++;
		}
		str += "</tr>";

		return str;
	}

	private static String buildAccordion(String parent, String name,
			String title) {
		String acc = "<div class=\"accordion-group\">"
				+ "<div class=\"accordion-heading\">"
				+ "	<a class=\"accordion-toggle\" data-toggle=\"collapse\""
				+ "		data-parent=\"#" + parent + "\" href=\"#" + name + "\"> "
				+ title + " </a>" + "</div>" + "<div id=\"" + name
				+ "\" class=\"accordion-body  collapse\""
				+ "	style=\"height: 0px;\">"
				+ "	<div class=\"accordion-inner\">" + "		<ul>";
		return acc;
	}

	private static String closeAccordion() {
		return ("		</ul>" + "	</div>" + "</div>" + "</div>");

	}

	public static Result javascriptRoutes() {
		response().setContentType("text/javascript");
		return ok(Routes.javascriptRouter("jsRoutes",
				routes.javascript.Application.getYearSpe(),
				routes.javascript.Application.nbMess(),
				routes.javascript.Application.updateScores()));
	}

}
