package models;

import java.io.File;
import java.util.Vector;

import controllers.Application;

public class GenerateContest {

	private Contestent constents1A = null;

	private Contestent constents2AIC = null;
	private Contestent constents2AICBE = null;
	private Contestent constents2AIMACS = null;
	private Contestent constents2AMIC = null;

	private Contestent constents3AIC = null;
	private Contestent constents3AICBE = null;
	private Contestent constents3AIMACS = null;
	private Contestent constents3AMIC = null;

	private Contestent constents4AGBA = null;
	private Contestent constents4AGC = null;
	private Contestent constents4AAE = null;
	private Contestent constents4AIR = null;
	private Contestent constents4AGM = null;
	private Contestent constents4AGMM = null;
	private Contestent constents4AGP = null;
	private Contestent constents4AGPE = null;

	private Contestent constents5AGBA = null;
	private Contestent constents5AGC = null;
	private Contestent constents5AAE = null;
	private Contestent constents5AIR = null;
	private Contestent constents5AGM = null;
	private Contestent constents5AGMM = null;
	private Contestent constents5AGP = null;
	private Contestent constents5AGPE = null;

	public GenerateContest(int tour) {
		System.out.println("Ici");
		if (Contestent.gitContestent().count("{tour: '"+tour+"'}") > 0)
		{
			System.out.println("La");
			File[] listOfFiles = Application.folder.listFiles();

			for (File file : listOfFiles) {
				if (file.isFile()
						&& (file.getName().endsWith(".jpg") || file.getName()
								.endsWith(".png"))) {
					String name = file.getName();
					if (name.contains("::1A.")) {
						this.constents1A = createContestents(this.constents1A,
								name, tour, "1A");
					} else if (name.contains("::2AMIC.")) {
						this.constents2AMIC = createContestents(
								this.constents2AMIC, name, tour, "2AMIC");
					} else if (name.contains("::2AIC.")) {
						this.constents2AIC = createContestents(
								this.constents2AIC, name, tour, "2AIC");
					} else if (name.contains("::2AICBE.")) {
						this.constents2AICBE = createContestents(
								this.constents2AICBE, name, tour, "2AICBE");
					} else if (name.contains("::2AIMACS.")) {
						this.constents2AIMACS = createContestents(
								this.constents2AIMACS, name, tour, "2AIMACS");
					}

					else if (name.contains("::3AMIC.")) {
						this.constents3AMIC = createContestents(
								this.constents3AMIC, name, tour, "3AMIC");
					} else if (name.contains("::3AIC.")) {
						this.constents3AIC = createContestents(
								this.constents3AIC, name, tour, "3AIC");
					} else if (name.contains("::3AICBE.")) {
						this.constents3AICBE = createContestents(
								this.constents3AICBE, name, tour, "3AICBE");
					} else if (name.contains("::3AIMACS.")) {
						this.constents3AIMACS = createContestents(
								this.constents3AIMACS, name, tour, "3AIMACS");
					}

					else if (name.contains("::4ABI.")) {
						this.constents4AGBA = createContestents(
								this.constents4AGBA, name, tour, "4ABI");
					} else if (name.contains("::4AAE.")) {
						this.constents4AAE = createContestents(
								this.constents4AAE, name, tour, "4AAE");
					} else if (name.contains("::4AIR.")) {
						this.constents4AIR = createContestents(
								this.constents4AIR, name, tour, "4AIR");
					} else if (name.contains("::4AGM.")) {
						this.constents4AGM = createContestents(
								this.constents4AGM, name, tour, "4AGM");
					} else if (name.contains("::4AMM.")) {
						this.constents4AGMM = createContestents(
								this.constents4AGMM, name, tour, "4AMM");
					} else if (name.contains("::4AGP.")) {
						this.constents4AGP = createContestents(
								this.constents4AGP, name, tour, "4AGP");
					} else if (name.contains("::4APR.")) {
						this.constents4AGPE = createContestents(
								this.constents4AGPE, name, tour, "4APR");
					} else if (name.contains("::4AGC.")) {
						this.constents4AGC = createContestents(
								this.constents4AGC, name, tour, "4AGC");
					}

					else if (name.contains("::5ABI.")) {
						this.constents5AGBA = createContestents(
								this.constents5AGBA, name, tour, "5ABI");
					} else if (name.contains("::5AAE.")) {
						this.constents5AAE = createContestents(
								this.constents5AAE, name, tour, "5AAE");
					} else if (name.contains("::5AIR.")) {
						this.constents5AIR = createContestents(
								this.constents5AIR, name, tour, "5AIR");
					} else if (name.contains("::5AGM.")) {
						this.constents5AGM = createContestents(
								this.constents5AGM, name, tour, "5AGM");
					} else if (name.contains("::5AMM.")) {
						this.constents5AGMM = createContestents(
								this.constents5AGMM, name, tour, "5AMM");
					} else if (name.contains("::5AGP.")) {
						this.constents5AGP = createContestents(
								this.constents5AGP, name, tour, "5AGP");
					} else if (name.contains("::5APR.")) {
						this.constents5AGPE = createContestents(
								this.constents5AGPE, name, tour, "5APR");
					} else if (name.contains("::5AGC.")) {
						this.constents5AGC = createContestents(
								this.constents5AGC, name, tour, "5AGC");
					}

				}
			}

			if (this.constents1A != null) {
				this.constents1A.insert();
			} else if (this.constents2AMIC != null) {
				this.constents2AMIC.insert();
			} else if (this.constents2AIC != null) {
				this.constents2AIC.insert();
			} else if (this.constents2AIC != null) {
				this.constents2AICBE.insert();
			} else if (this.constents2AIC != null) {
				this.constents2AIMACS.insert();
			}

			else if (this.constents3AMIC != null) {
				this.constents3AMIC.insert();
			} else if (this.constents3AIC != null) {
				this.constents3AIC.insert();
			} else if (this.constents3AICBE != null) {
				this.constents3AICBE.insert();
			} else if (this.constents3AIMACS != null) {
				this.constents3AIMACS.insert();
			}

			else if (this.constents4AGBA != null) {
				this.constents4AGBA.insert();
			} else if (this.constents4AAE != null) {
				this.constents4AAE.insert();
			} else if (this.constents4AIR != null) {
				this.constents4AIR.insert();
			} else if (this.constents4AGM != null) {
				this.constents4AGM.insert();
			} else if (this.constents4AGMM != null) {
				this.constents4AGMM.insert();
			} else if (this.constents4AGP != null) {
				this.constents4AGP.insert();
			} else if (this.constents4AGPE != null) {
				this.constents4AGPE.insert();
			} else if (this.constents4AGC != null) {
				this.constents4AGC.insert();
			}

			else if (this.constents5AGBA != null) {
				this.constents5AGBA.insert();
			} else if (this.constents5AAE != null) {
				this.constents5AAE.insert();
			} else if (this.constents5AIR != null) {
				this.constents5AIR.insert();
			} else if (this.constents5AGM != null) {
				this.constents5AGM.insert();
			} else if (this.constents5AGMM != null) {
				this.constents5AGMM.insert();
			} else if (this.constents5AGP != null) {
				this.constents5AGP.insert();
			} else if (this.constents5AGPE != null) {
				this.constents5AGPE.insert();
			} else if (this.constents5AGC != null) {
				this.constents5AGC.insert();
			}

		}
	}

	private Contestent createContestents(Contestent contest, String name,
			int tour, String spe) {
		if (contest != null) {
			contest.addContestent2(name);
			contest.insert();
			contest = null;
		} else {
			contest = new Contestent();
			contest.contestent(tour, name, spe);
		}
		return contest;
	}
}
