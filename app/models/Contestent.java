package models;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

public class Contestent {

	private static int cptContesent = 0;;
	@JsonProperty("_id")
	public ObjectId id;

	@JsonProperty("num")
	public int num;

	@JsonProperty("imgConstes1")
	public String imgConstes1;

	@JsonProperty("imgConstes2")
	public String imgConstes2;

	@JsonProperty("scoreC1")
	public int scoreC1;

	@JsonProperty("scoreC2")
	public int scoreC2;

	@JsonProperty("tour")
	public int tour;

	@JsonProperty("specialite")
	public String specialite;

	private static Mongo mongo = null;
	public static Jongo jongo = null;

	public Contestent() {
		this.imgConstes1 = "COMPAN_AUDE_::1A.jpg";
		this.imgConstes2 = "NIARFEIX_CLARA_::1A.jpg";
	}

	public void contestent(int tour, String contestent1, String specialite) {
		this.imgConstes1 = contestent1;
		this.imgConstes2 = null;
		this.tour = tour;
		this.scoreC1 = 0;
		this.scoreC2 = 0;
		this.specialite = specialite;
		this.num = Contestent.cptContesent++;
	}

	public void addContestent2(String contestent2) {
		this.imgConstes2 = contestent2;
	}

	public static void createPlayJongo() throws UnknownHostException,
			MongoException {
		if (mongo == null || jongo == null) {
			mongo = new Mongo("localhost", 27017);
			jongo = new Jongo(mongo.getDB("GIT"));
		}
	}

	public static MongoCollection gitContestent() {
		try {
			createPlayJongo();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MongoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jongo.getCollection("GIT");
	}

	public void insert() {
		gitContestent().save(this);
	}

	public static <T> List<T> copyIterator(Iterator<T> iter) {
		List<T> copy = new ArrayList<T>();
		while (iter.hasNext())
			copy.add(iter.next());
		return copy;
	}

	public void incrementScoreContestent1() {
		gitContestent().update(
				"{imgConstes1: '" + this.imgConstes1 + "', tour: " + this.tour + "}")
				.with("{$inc: {scoreC1: 1}}");

	}

	public void incrementScoreContestent2() {
		gitContestent().update(
				"{imgConstes2: '" + this.imgConstes2 + "', tour: " + this.tour + "}")
				.with("{$inc: {scoreC2: 1}}");
		

	}

	public void remove() {
		gitContestent().remove(this.id);
	}

	public static Contestent findByName(String imgConstes1, String imgConstes2,
			int tour) {
		return gitContestent().findOne("{imgConstes1: #, imgConstes2: #, tour: #}",
				imgConstes1, imgConstes2, tour).as(Contestent.class);
	}

	public static Contestent findByNum(int tour, int num) {
		return gitContestent().findOne("{num: #, tour: #}", num, tour).as(
				Contestent.class);
	}

	public static Iterable<Contestent> findBySpe(int tour, String specialite) {
		return gitContestent().find("{specialite: #, tour: #}", specialite, tour)
				.as(Contestent.class);
	}

}
