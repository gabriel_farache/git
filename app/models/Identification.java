package models;

import java.net.UnknownHostException;
import java.util.*;

import org.bson.types.ObjectId;
import org.jongo.Jongo;
import org.jongo.MongoCollection;

import play.data.validation.Constraints.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.typesafe.plugin.*;

public class Identification {


	public String name;

	public String pswd;

	public static String auth(Identification ident) {
		System.out.println(ident.name);
		String mdp = Identification.generatePSWD();
		/*
		 * MailerAPI mail =
		 * play.Play.application().plugin(MailerPlugin.class).email();
		 * mail.setSubject("mailer"); mail.addRecipient(ident.name);
		 * mail.addFrom("ginsat.vs@gmail.com"); //sends text/text mail.send(
		 * "Ton mot de passe : "+mdp); //sends both text and html
		 */

		return mdp;

	}

	private static String generatePSWD() {
		String keylist = "abcdefghijklmnopqrstuvwxyz123456789";

		String temp = "";
		for (int i = 0; i < 10; i++)
			temp += keylist.charAt((int) Math.floor(Math.random()
					* keylist.length()));
		return temp;
	}

	public static String mdp(Identification ident) {
		System.out.println(ident.pswd);

		return ident.pswd;

	}



	public static MongoCollection gitVotant() {
		try {
			Contestent.createPlayJongo();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MongoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Contestent.jongo.getCollection("GIT_votants");
	}

	public static Boolean hasAlreadyVoted(String name, ObjectId contesId) {
		Boolean hasAlreadyVoted = true;

		hasAlreadyVoted = (gitVotant()
				.findOne("{contesId: #, name: #}", contesId, name)
				.as(Votant.class)) != null;

		return hasAlreadyVoted;
	}

	public static void insertVotant(Votant v) {
		gitVotant().save(v);
	}

	public static class Votant {
		@JsonProperty("_id")
		public ObjectId id;
		@JsonProperty("name")
		public String name;
		@JsonProperty("contesId")
		public ObjectId contesId;

		public Votant() {
			
		}
		public Votant(String name, ObjectId contesId) {
			super();
			this.name = name;
			this.contesId = contesId;
		}
	}
}