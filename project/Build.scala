import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "GIT"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    javaCore,
    "com.typesafe" % "play-plugins-mailer_2.10" % "2.1-SNAPSHOT",
    "uk.co.panaxiom" %% "play-jongo" % "0.4"

  )

  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here
    resolvers += Resolver.url("My GitHub Play Repository", url("http://alexanderjarvis.github.com/releases/"))(Resolver.ivyStylePatterns)
  )

}
